# Mainteiner: Richard Lees <git zero at bitservices dot io>
###############################################################################

_pkgname=spotify-player

###############################################################################

pkgname=${_pkgname}-gstreamer
pkgver=0.20.4
pkgrel=2
pkgdesc="A command driven spotify player with gstreamer backend."
arch=('x86_64' 'aarch64' 'armv7h')
url="https://github.com/aome510/spotify-player"
license=('MIT')
options=(!lto)
depends=('dbus'
         'ghostty'
         'glibc'
         'gst-plugins-base'
         'openssl')
makedepends=('cargo'
             'gcc-libs')
source=("${_pkgname}-${pkgver}.tar.gz::${url}/archive/v${pkgver}.tar.gz"
        "${pkgname}.desktop"
        "${pkgname}.svg")
sha256sums=('1d13f47ef4df3415835736f32629d57e331707d781507007ea04217a7dc735d8'
            'dea229d3c28bfc4b5a7357aa20e1ef73c9651da4d5007a69fe51d42a808431c8'
            '6f52ef55ab68a008c224739154f84f33bc91ae1dd4739c8cdb464c1776268582')

###############################################################################

build() {
    cd "${_pkgname}-${pkgver}"
    cargo build --release --no-default-features --locked --features gstreamer-backend,media-control,sixel,notify
}

###############################################################################

package() {
    install -Dm755 "${_pkgname}-${pkgver}/target/release/spotify_player" -t "${pkgdir}/usr/bin"
    install -Dm644 "${_pkgname}-${pkgver}/LICENSE" "${pkgdir}/usr/share/licenses/${pkgname}/LICENSE"
    install -Dm644 "${pkgname}.desktop" "${pkgdir}/usr/share/applications/${pkgname}.desktop"
    install -Dm644 "${pkgname}.svg" "${pkgdir}/usr/share/icons/hicolor/scalable/apps/${pkgname}.svg"
}

###############################################################################
